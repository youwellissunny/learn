package com.sunny.structure;

import com.sunny.structure.array.MyArray;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by lijinlong on 2017/6/26.
 */
public class TestArray {

    @Test
    public void test4(){
        MyArray array = new MyArray(10);
        array.insertByOrder(20);
        array.insertByOrder(19);
        array.insertByOrder(21);
        array.insertByOrder(2);
        array.insertByOrder(50);
        array.display();

        int find = array.binaryFind(23);
        System.out.println(find);
    }


    @Test
    public void test3(){
        MyArray array = new MyArray();
        array.insert(10);
        array.insert(20);
        array.insert(20);
        array.insert(20);
        array.insert(25);
        array.insert(20);
        array.display();
        int i = array.find(29);
        System.out.println(i);
    }

    /**
     * 操作数组中的数据
     */
    @Test
    public void test2(){

        int[] intArr = new int[10];
        intArr[8] = 10;
        System.out.println(Arrays.toString(intArr));

        for (int i : intArr) {
            System.out.println(i);
        }

        for (int i = 0; i < intArr.length; i++) {
            intArr[i] = i;
        }




    }

    /**
     * 创建和初始化数组
     */
    @Test
    public void test1(){
        int[] arr1 = new int[10];
        arr1[0] = 12;
        System.out.println(Arrays.toString(arr1));

        char[] charArr = new char[10];
        System.out.println(Arrays.toString(charArr));

        float[] floatArr = new float[10];
        System.out.println(Arrays.toString(floatArr));


        int[] intarr = new int[]{};
        System.out.println(intarr.length);
    }

}
