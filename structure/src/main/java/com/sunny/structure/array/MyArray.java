package com.sunny.structure.array;

import sun.reflect.generics.tree.VoidDescriptor;

/**
 * Created by lijinlong on 2017/6/26.
 * 面向对象操作数组
 */
public class MyArray {

    private long[] arr;

    //数组有效数据长度
    private int elems;

    public MyArray(){
        arr = new long[10];
    }

    public MyArray(int length){
        arr = new long[length];
    }


    public void insertByOrder(long value){

        int i =0;
        for (i = 0; i < elems; i++) {
            if (arr[i] > value){
                break;
            }
        }

        for (int j=elems; j>i; j--){
            arr[j] = arr[j-1];
        }

        arr[i] = value;
        elems++;

    }

    /**
     * 插入数据
     * @param value
     */
    public void insert(long value){
        arr[elems] = value;
        elems++;
    }

    /**
     * 显示数据
     */
    public void display(){
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
    }

    /**
     * 查找方法, 返回下标
     * @param value
     * @return
     */
    public int find(long value){
        for (int i = 0; i < elems; i++) {
            if (value == arr[i]){
                return i;
            }
        }
        System.out.println("没有找到数据");
        return -1;
    }

    /**
     * 二分法查找
     * 数组中的元素必须是按照一定的顺序进行存储的,
     * @return
     */
    public int binaryFind(long value){

        int ins = 0;
        int start = 0;
        int end = elems;

        while (true){
            ins = (start + end)/2;//取中间值
            if (arr[ins] == value){
                return ins;
            }else if (start > end){
                return  -1;
            }else {
                //将要查找的值与中间值进行比较, 根据比较结果重新界定最大与最小下标
                if (arr[ins] > value){
                    end = ins - 1;
                }else {
                    start = ins + 1;
                }
            }
        }

    }

}
