package com.sunny.structure.array;

import java.util.Arrays;

/**
 * Created by lijinlong on 2017/7/5.
 */
public class BubbleSort {


    public static void main(String[] args) {
        int[] arr = new int[]{10,18,2,5,17,0,5};
        bubbleSortAsc(arr);
        bubbleSortDesc(arr);
    }

    /**
     * 冒泡排序
     * @param arr
     */
    public static void bubbleSortAsc(int[] arr){


        //因为最后一个元素是用+1去取, 所以循环要-1
        for (int i = 0; i < arr.length - 1; i++) {

            //外层循环一次后, 最大值已经排在最后一个了, 所以要减去i
            for (int j = 0; j < arr.length - i - 1; j++) {
                //比较两个相邻元素
                if (arr[j] > arr[j+1]){
                    swap(arr, j, j+1);
                }
            }
            
        }

        System.out.println(Arrays.toString(arr));
        

    }

    public static void bubbleSortDesc(int[] arr){
        //因为最后一个元素是用+1去取, 所以循环要-1
        for (int i = 0; i < arr.length - 1; i++) {

            //外层循环一次后, 最大值已经排在最后一个了, 所以要减去i
            for (int j = 0; j < arr.length - i - 1; j++) {
                //比较两个相邻元素
                if (arr[j] < arr[j+1]){
                    swap(arr, j, j+1);
                }
            }

        }

        System.out.println(Arrays.toString(arr));


    }

    /**
     * 交换数组两个值
     * @param a
     * @param b
     */
    public static void swap(int[] arr, int a, int b){
        int temp = 0;
        temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

}
