package com.sunny;

/**
 * Created by lijinlong on 2017/4/11.
 */
public class InterruptedTest extends Thread{
    @Override
    public void run() {
        int count = 0;
        while (count < 1000) {
            System.out.println(count+"----------"+Thread.currentThread().getName());
            System.out.println(Thread.currentThread().isInterrupted());
            if (count == 5) {
                System.out.println("---------------------");
                Thread.currentThread().interrupt();
                System.out.println("--------------------"+Thread.currentThread());
            }
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("this currentThread is interrupted");
                break;
            }
            count ++ ;
        }
    }

    public static void test(String[] args) {
        System.out.println("zheshiyigetest");
    }

    public static void main(String[] args) {
        InterruptedTest.test(null);
        InterruptedTest test = new InterruptedTest();
        Thread thread = new Thread(test);
        System.out.println(Thread.currentThread().getName());
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            thread.interrupt();
        }
        System.out.println("-----------------------主线程结束");

    }
}
