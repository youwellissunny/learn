package com.sunny.thread;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by lijinlong on 2017/5/9.
 */
public class ReentrantLockTest implements  Runnable{

    public  static ReentrantLock lock = new ReentrantLock();

    public static  int i;

    public void run() {
        for (int j = 0; j < 100000; j++) {
            lock.lock();
            try {
                i++;
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args)  throws  Exception{
        ReentrantLockTest test = new ReentrantLockTest();
        Thread t1 = new Thread(test);
        Thread t2 = new Thread(test);

        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(i);
    }
}
